FROM golang:1.10-alpine
MAINTAINER arnon kijlerdphon (snappy.kop@gmail.com)
# ADD ./goapp /go/src/Api-For-Flutter/goapp
RUN mkdir -p /go/src/Api-For-Flutter/goapp \
    && apk add --update tzdata \
    && cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
    && apk del tzdata \
    && apk add --no-cache git curl \
    && rm -rf /var/cache/apk/* \
    && set -x \
    && curl https://glide.sh/get | sh \
    && go get -v github.com/revel/revel \
    && go get -v github.com/revel/cmd/revel
WORKDIR /go/src/Api-For-Flutter/goapp
EXPOSE 9000