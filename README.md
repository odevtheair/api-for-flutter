# Example API for Flutter 101 course

# Develop In Revel Framework

A high-productivity web framework for the [Go language](http://www.golang.org/).

### Reproducible Installations

Install the [dependencies](https://glide.sh/) and revisions listed in the lock file into the vendor directory. If no lock file exists an update is run.

```
$ glide https://glide.sh/ install
```

### Start the web server:

```
docker-compose build
docker-compose up -d
docker-compose exec app glide install
```

### Go to http://localhost:9000/ and you'll see:

| Username     | Password    |
| -------------|-------------|
| admin        | password    |

![alt text](https://github.com/aofiee/Omise-Go-Example/blob/master/public/img/loginScreen.png?raw=true)

## Config app.conf
        [dev]
        db.driver = mysql
        db.protocol = tcp
        # db.hostname = db
        db.hostname = db
        db.port = 3306
        db.username = flutter_u
        db.password = h8Org8nfN1
        db.database_name = "flutter_leaning_db"
        db.timezone = "Asia/Bangkok"
        db.charset = utf8
        db.collation = utf8_bin     
        [pro]
        db.driver = mysql
        db.protocol = tcp
        # db.hostname = db
        db.hostname = localhost
        db.port = 3306
        db.username = flutter_u
        db.password = h8Org8nfN1
        db.database_name = "flutter_leaning_db"
        db.timezone = "Asia/Bangkok"
        db.charset = utf8
        db.collation = utf8_bin

ในไฟล์ app/init.go ให้สร้าง database และ ข้อมูลขั้นต้นด้วย method 

        revel.OnAppStart(models.InitDB)

## Code Layout

The directory structure of a generated Revel application:

    conf/             Configuration directory
        app.conf      Main app configuration file
        routes        Routes definition file

    app/              App sources
        init.go       Interceptor registration
        controllers/  App controllers go here
        views/        Templates directory
        models/       Type Struct

    messages/         Message files

    public/           Public static assets
        css/          CSS files
        js/           Javascript files
        images/       Image files

    tests/            Test suites


## UnitTest

http://localhost:9000/@tests

## revel build
    $ glide get github.com/revel/cmd/revel
    $ revel package Api-For-Flutter