package controllers

import (
	"github.com/revel/revel"
)

var hmacSecret = []byte{61, 56, 115, 83, 90, 56, 116, 125, 78, 110, 38, 113, 122, 93, 90, 54, 54, 84, 96, 104, 62, 62, 76, 59, 35, 37, 85, 72, 74, 50, 70, 83}

func init() {
	/*

	   +-----------------------------------------------------------+
	   |                                                           |
	   |            Auto Check Permission Middleware               |
	   |                                                           |
	   +-----------------------------------------------------------+

	*/
	revel.InterceptMethod(MdMobile.checkUser, revel.BEFORE)
	/*

	   +-----------------------------------------------------------+
	   |                                                           |
	   |            Auto Check Permission Middleware               |
	   |                                                           |
	   +-----------------------------------------------------------+

	*/
}
