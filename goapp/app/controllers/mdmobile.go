package controllers

/* Package controllers for startup program
+-------------------------------------------------------------------------------------------+
|                                                                                           |
|                                                                                           |
|      								                                                        |
|                                                                                           |
|                                        .',,;,,..                                          |
|                                   ;d0NMMMMMMMMMMWXOo;.                                    |
|                                ;OWMMMMMMMMMMMMMMMMMMMMKo.                                 |
|                              cXMMMMMMMMMMMMMMMMMMMMMMMMMM0,                               |
|                            cXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM0.                             |
|                          .KMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWc                            |
|                          dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM;                           |
|                          XMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMO                           |                           -
|                          XMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMX                           |
|                          XMMc:xXMMMMMMMMMMMMMMMMMMMMMMMNOo:cMMO                           |
|                          0MM,   .;dKWMMMMMMMMMMMMMMWKo'    'MM;                           |
|                          lMMK.      .;dXMMMMMMMMNkc.       0MK                            |
|                           OMMXl,.       'OMMMM0,       .'cKMMc                            |
|                            cNMMMMMNK0O0NMMXxNXMWX0O0KNWMMMMMX                             |
|                            :KMMMMMMMMMMMMK.kk.OMMMMMMMMMMMMMx                             |
|                            NMMMMMMXkdXMMMxoMMooMMMMMNOddXMMMW.                            |
|                            OMMMMk'   kMMMMMMMMMMMMMN    lMMO'                             |
|                             ;:..c.   OMMMMMWWMNoNMMx    .;.                               |
|                                      'MMkWMOoMKlMMW.                                      |
|                                       WMdXMklMNxMMx                                       |
|                                       WMd0MkcMWkMMo                                       |
|                                       NMd0MkcMNxMMl                                       |
|                                       XMd0Mk:MXdMM;                                       |
|                                       0Md0Mk:MOoMM.                                       |
|                                       lWoOMx;MloM0                                        |
|                                          ':. :..'                                         |
|                                                                                           |
+-------------------------------------------------------------------------------------------+
*/
import (
	"fmt"

	"github.com/revel/revel"
)

//MdMobile type
type MdMobile struct {
	*revel.Controller
	App
}

//checkUser func
func (c MdMobile) checkUser() revel.Result {
	fmt.Println("MdMobile checkUser")
	auth, err, response := c.AuthenticateMobile()
	if !auth {
		return c.ReturnHeaderAndResponse(response, err.Error(), nil)
	}
	return nil
}
