package controllers

/* Package controllers for startup program
+-------------------------------------------------------------------------------------------+
|                                                                                           |
|                                                                                           |
|      								                                                        |
|                                                                                           |
|                                        .',,;,,..                                          |
|                                   ;d0NMMMMMMMMMMWXOo;.                                    |
|                                ;OWMMMMMMMMMMMMMMMMMMMMKo.                                 |
|                              cXMMMMMMMMMMMMMMMMMMMMMMMMMM0,                               |
|                            cXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM0.                             |
|                          .KMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWc                            |
|                          dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM;                           |
|                          XMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMO                           |                           -
|                          XMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMX                           |
|                          XMMc:xXMMMMMMMMMMMMMMMMMMMMMMMNOo:cMMO                           |
|                          0MM,   .;dKWMMMMMMMMMMMMMMWKo'    'MM;                           |
|                          lMMK.      .;dXMMMMMMMMNkc.       0MK                            |
|                           OMMXl,.       'OMMMM0,       .'cKMMc                            |
|                            cNMMMMMNK0O0NMMXxNXMWX0O0KNWMMMMMX                             |
|                            :KMMMMMMMMMMMMK.kk.OMMMMMMMMMMMMMx                             |
|                            NMMMMMMXkdXMMMxoMMooMMMMMNOddXMMMW.                            |
|                            OMMMMk'   kMMMMMMMMMMMMMN    lMMO'                             |
|                             ;:..c.   OMMMMMWWMNoNMMx    .;.                               |
|                                      'MMkWMOoMKlMMW.                                      |
|                                       WMdXMklMNxMMx                                       |
|                                       WMd0MkcMWkMMo                                       |
|                                       NMd0MkcMNxMMl                                       |
|                                       XMd0Mk:MXdMM;                                       |
|                                       0Md0Mk:MOoMM.                                       |
|                                       lWoOMx;MloM0                                        |
|                                          ':. :..'                                         |
|                                                                                           |
+-------------------------------------------------------------------------------------------+
*/
import (
	"Api-For-Flutter/goapp/app/models"
	"fmt"
	"math/rand"
	"net/http"

	"github.com/revel/revel"
)

//Mobile type
type Mobile struct {
	*revel.Controller
	MdMobile
}

//ResponseStartLimit type
type ResponseStartLimit struct {
	Start int
	Limit int
	Total int
	Obj   []ResponseObj
}

//ResponseObj type
type ResponseObj struct {
	ID          uint
	Name        string
	Description string
	PathImage   string
}

//ResponseUser type
type ResponseUser struct {
	Username    string
	Email       string
	Description string
	PathImage   string
}

//ResponseTest type
type ResponseTest struct {
	ID   uint
	Name string
}

// random func
func random(min int, max int) int {
	return rand.Intn(max-min) + min
}

//ListTest func
func (c Mobile) ListTest() revel.Result {
	var test []ResponseTest
	db := models.Gorm
	db.Table("test_struct").Select("id, name").Find(&test)
	return c.ReturnHeaderAndResponse(http.StatusOK, "ดึงข้อมูลสำเร็จ", test)
}

//CreateTest func
func (c Mobile) CreateTest(name string) revel.Result {
	c.Validation.Required(name).Message("กรุณากรอกข้อมูล name")
	if c.Validation.HasErrors() {
		resultError := c.Validation.ErrorMap()
		var errText string
		for _, err := range resultError {
			errText = err.Message
			break
		}
		// log.Println(errText)
		// log.Println(result)
		return c.ReturnHeaderAndResponse(http.StatusNotAcceptable, errText, nil)
	}

	db := models.Gorm
	test := &models.TestStruct{
		Name: name,
	}
	db.Create(&test)

	var testResponse ResponseTest
	testResponse.ID = test.ID
	testResponse.Name = test.Name
	return c.ReturnHeaderAndResponse(http.StatusOK, "บันทึกข้อมูลสำเร็จ", testResponse)
}

//UpdateTest func
func (c Mobile) UpdateTest(tid int, name string) revel.Result {
	c.Validation.Required(name).Message("กรุณากรอกข้อมูล name")
	if c.Validation.HasErrors() {
		resultError := c.Validation.ErrorMap()
		var errText string
		for _, err := range resultError {
			errText = err.Message
			break
		}
		// log.Println(errText)
		// log.Println(result)
		return c.ReturnHeaderAndResponse(http.StatusNotAcceptable, errText, nil)
	}

	db := models.Gorm
	var test models.TestStruct
	db.Where("id = ?", tid).Find(&test)
	if test.ID != 0 {
		test.Name = name
		db.Save(&test)

		var testResponse ResponseTest
		testResponse.ID = test.ID
		testResponse.Name = test.Name
		return c.ReturnHeaderAndResponse(http.StatusOK, "แก้ไขข้อมูลสำเร็จ", testResponse)
	}
	return c.ReturnHeaderAndResponse(http.StatusNotFound, "ID นี้ไม่มีข้อมูล", nil)
}

//DeleteTest func
func (c Mobile) DeleteTest(tid int) revel.Result {
	db := models.Gorm
	var test models.TestStruct
	db.Where("id = ?", tid).Find(&test)
	if test.ID != 0 {
		db.Delete(&test)

		var testResponse ResponseTest
		testResponse.ID = test.ID
		testResponse.Name = test.Name
		return c.ReturnHeaderAndResponse(http.StatusOK, "ลบข้อมูลสำเร็จ", testResponse)
	}
	return c.ReturnHeaderAndResponse(http.StatusNotFound, "ID นี้ไม่มีข้อมูล", nil)
}

//DetailTest func
func (c Mobile) DetailTest(tid int) revel.Result {
	db := models.Gorm
	var test ResponseTest
	db.Table("test_struct").Where("id = ?", tid).Select("id, name").First(&test)
	fmt.Println(test)
	if test.ID != 0 {
		return c.ReturnHeaderAndResponse(http.StatusOK, "ลบข้อมูลสำเร็จ", test)
	}
	return c.ReturnHeaderAndResponse(http.StatusNotFound, "ID นี้ไม่มีข้อมูล", nil)
}

//GetPathImageObj func
func (c Mobile) GetPathImageObj(PathImage string) string {
	if PathImage == "" {
		PathImage = "img-whoops.jpg"
	}

	if revel.HTTPSsl == true {
		PathImage = "https://" + c.Request.Host + "/public/uploads/obj/" + PathImage
	} else {
		PathImage = "http://" + c.Request.Host + "/public/uploads/obj/" + PathImage
	}
	return PathImage
}

//GetPathImageUser func
func (c Mobile) GetPathImageUser(PathImage string) string {
	if PathImage == "" {
		PathImage = "default_users.jpg"
	}

	if revel.HTTPSsl == true {
		PathImage = "https://" + c.Request.Host + "/public/uploads/usres/" + PathImage
	} else {
		PathImage = "http://" + c.Request.Host + "/public/uploads/usres/" + PathImage
	}
	return PathImage
}

//IndexObj func
func (c Mobile) IndexObj(start int, limit int) revel.Result {
	// path := revel.BasePath + "/public/uploads/temp-userimage/"
	// files, err := ioutil.ReadDir(path)
	// if err != nil {
	// 	log.Println(err)
	// }
	// obj := [...]string{
	// 	"CanBNK48.jpg",
	// 	"CherprangBNK48.jpg",
	// 	"JaaBNK48.jpg",
	// 	"JanBNK48.jpg",
	// 	"JaneBNK48.jpg",
	// 	"JennisBNK48.jpg",
	// 	"JibBNK48.jpg",
	// 	"KaewBNK48.jpg",
	// 	"KaimookBNK48.jpg",
	// 	"KornBNK48.jpg",
	// 	"MaysaBNK48.jpg",
	// 	"MindBNK48.jpg",
	// 	"MioriBNK48.jpg",
	// 	"MobileBNK48.jpg",
	// 	"MusicBNK48.jpg",
	// 	"NamnuengBNK48.jpg",
	// 	"NamsaiBNK48.jpg",
	// 	"NinkBNK48.jpg",
	// 	"NoeyBNK48.jpg",
	// 	"OrnBNK48.jpg",
	// 	"PiamBNK48.jpg",
	// 	"PunBNK48.jpg",
	// 	"PupeBNK48.jpg",
	// 	"RinaBNK48.jpg",
	// 	"SatChanBNK48.jpg",
	// 	"TarwannBNK48.jpg",
	// }
	// i := 1
	// for _, f := range files {
	// 	name := f.Name()
	// 	extension := filepath.Ext(name)
	// 	if extension == ".jpg" {
	// 		obj = append(obj, name)
	// 		i++
	// 	}
	// }
	// for index, f := range files {
	// 	name := f.Name()
	// 	extension := filepath.Ext(name)
	// 	if extension == ".jpg" {
	// 		log.Println(strconv.Itoa(index))
	// 		log.Println(f.Name())
	// 		// log.Println(filepath.Ext(f.Name()))
	// 		// before := path + name
	// 		// after := path + strconv.Itoa(i) + extension
	// 		// os.Rename(before, after)

	// 		file, err := os.Open(path + name)
	// 		if err != nil {
	// 			log.Fatal(err)
	// 		}

	// 		// // decode jpeg into image.Image
	// 		img, err := jpeg.Decode(file)
	// 		if err != nil {
	// 			log.Fatal(err)
	// 		}
	// 		file.Close()

	// 		// // resize to width 1000 using Lanczos resampling
	// 		// // and preserve aspect ratio
	// 		m := resize.Resize(600, 400, img, resize.Lanczos3)

	// 		out, err := os.Create(path + "resize/" + name)
	// 		if err != nil {
	// 			log.Fatal(err)
	// 		}
	// 		defer out.Close()

	// 		// // write new image to file
	// 		jpeg.Encode(out, m, nil)
	// 		i++
	// 	}

	// }
	// text := [...]string{"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
	// 	"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
	// 	"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus Bonorum et Malorum' (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, 'Lorem ipsum dolor sit amet..', comes from a line in section 1.10.32.",
	// 	"There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.",
	// 	"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	// 	"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
	// 	"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?",
	// 	"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.",
	// 	"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.",
	// }
	// var myrand int
	// for _, f := range files {
	// 	fileName := f.Name()
	// 	extension := filepath.Ext(fileName)
	// 	myrand = random(0, 8)
	// 	fmt.Println(myrand)
	// 	if extension == ".jpg" {
	// 		db := models.Gorm
	// 		obj := &models.ObjStruct{
	// 			Name:        "test" + strconv.Itoa(i),
	// 			Description: text[myrand],
	// 			PathImage:   strconv.Itoa(i) + ".jpg",
	// 		}
	// 		db.Create(&obj)
	// 		i++
	// 	}
	// }
	if start <= 0 {
		start = 0
	}
	if limit <= 0 {
		limit = 10
	}
	var count int
	var obj []ResponseObj
	var response ResponseStartLimit
	db := models.Gorm
	db.Table("obj_struct").Select("id, name, description, path_image").Limit(limit).Offset(start).Find(&obj).Count(&count)
	for i := range obj {
		obj[i].PathImage = c.GetPathImageObj(obj[i].PathImage)
	}

	response.Start = start
	response.Limit = limit
	response.Total = count
	response.Obj = obj
	return c.ReturnHeaderAndResponse(http.StatusOK, "ดึงข้อมูลสำเร็จ", response)
}

//GetObjDetail func
func (c Mobile) GetObjDetail(oid int) revel.Result {
	var obj ResponseObj
	db := models.Gorm
	db.Table("obj_struct").Where("id = ?", oid).Select("id, name, description, path_image").First(&obj)
	if obj.ID != 0 {
		obj.PathImage = c.GetPathImageObj(obj.PathImage)
		return c.ReturnHeaderAndResponse(http.StatusOK, "ดึงข้อมูลสำเร็จ", obj)
	}
	return c.ReturnHeaderAndResponse(http.StatusNotFound, "ID นี้ไม่มีข้อมูล", nil)
}

//IndexUsers func
func (c Mobile) IndexUsers() revel.Result {
	var user []ResponseUser
	db := models.Gorm
	db.Table("user_struct").Select("email, username, description, path_image").Find(&user)
	for i := range user {
		user[i].PathImage = c.GetPathImageUser(user[i].PathImage)
	}
	return c.ReturnHeaderAndResponse(http.StatusOK, "ดึงข้อมูลสำเร็จ", user)
}
