package controllers

import (
	"Api-For-Flutter/goapp/app/helpers"
	"Api-For-Flutter/goapp/app/models"
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"io/ioutil"
	"log"
	mathRand "math/rand"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"mime/multipart"
	"os"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/nfnt/resize"
	"github.com/oklog/ulid"
	"github.com/revel/revel"
	"golang.org/x/crypto/pbkdf2"
)

// App Struct
type App struct {
	*revel.Controller
}

//AuthenticationMobile struct
type AuthenticationMobile struct {
	Token string
}

//JSONResult struct
type JSONResult struct {
	Status  int
	Message string
}

//UploadResultMessage struct
type UploadResultMessage struct {
	fileNameSaved *string
	message       string
	completed     bool
}

func genUlid() string {
	t := time.Now().UTC()
	entropy := mathRand.New(mathRand.NewSource(t.UnixNano()))
	id := ulid.MustNew(ulid.Timestamp(t), entropy)
	return id.String()
}

func deriveKey(passphrase string, salt []byte) ([]byte, []byte) {
	if salt == nil {
		salt = make([]byte, 8)
		rand.Read(salt)
	}
	return pbkdf2.Key([]byte(passphrase), salt, 1000, 32, sha256.New), salt
}

func encrypt(passphrase, plaintext string) string {
	key, salt := deriveKey(passphrase, nil)
	iv := make([]byte, 12)
	rand.Read(iv)
	b, _ := aes.NewCipher(key)
	aesgcm, _ := cipher.NewGCM(b)
	data := aesgcm.Seal(nil, iv, []byte(plaintext), nil)
	return hex.EncodeToString(salt) + "-" + hex.EncodeToString(iv) + "-" + hex.EncodeToString(data)
}

func decrypt(passphrase, ciphertext string) string {
	arr := strings.Split(ciphertext, "-")
	salt, _ := hex.DecodeString(arr[0])
	iv, _ := hex.DecodeString(arr[1])
	data, _ := hex.DecodeString(arr[2])
	key, _ := deriveKey(passphrase, salt)
	b, _ := aes.NewCipher(key)
	aesgcm, _ := cipher.NewGCM(b)
	data, _ = aesgcm.Open(nil, iv, data, nil)
	return string(data)
}

//MethodOptions metthod
func (c App) MethodOptions() revel.Result {
	return c.ReturnHeaderAndResponse(http.StatusOK, "It' ok.", nil)
}

// AuthenticateMobile func
func (c App) AuthenticateMobile() (bool, error, int) {
	fmt.Println("App AuthenticateMobile")
	tokenString, err := helpers.GetTokenString(c.Request.Header.Get("Authorization"))
	if err != nil {
		log.Println("get token string failed")
		return false, errors.New("get token string failed"), http.StatusUnauthorized
	}
	var claims jwt.MapClaims
	claims, err = helpers.DecodeToken(tokenString)
	if err != nil {
		return false, err, http.StatusUnauthorized
	}
	// log.Println(claims)
	encode, found := claims["code"]
	if !found {
		log.Println(err)
		return false, err, http.StatusUnauthorized
	}

	str, _ := encode.(string)
	secret, _ := revel.Config.String("app.secret")
	uid := decrypt(secret, str)

	log.Println("encode string:", encode)
	log.Println("uid found:", uid)

	db := models.Gorm
	// user := &models.UserTokenStruct{}
	var count int
	db.Table("user_token_struct").Where("user_struct_refer = ?", uid).Where("token = ?", tokenString).Count(&count)
	if count == 0 {
		return false, errors.New("ไม่พบข้อมูลผู้ใช้งาน"), http.StatusNotFound
	}
	log.Println("auth token success")
	return true, nil, http.StatusOK
}

func (c App) uploadImage(imageFile io.Reader,
	f map[string][]*multipart.FileHeader) UploadResultMessage {

	log.Println("imageFile")
	log.Println(f["imageFile"])
	headers, ok := f["imageFile"]
	if ok {
		if len(headers) != 1 {
			log.Println("คุณส่งข้อมูลรูปภาพเข้ามาซ้ำกันในระบบ")

			return UploadResultMessage{
				fileNameSaved: nil,
				message:       "คุณส่งข้อมูลรูปภาพเข้ามาซ้ำกันในระบบ",
				completed:     false,
			}
		}
		var (
			err           error
			bildBuf       bytes.Buffer
			nameSplit     []string
			suffix        string
			uploadDaten   image.Image
			newFile       *os.File
			fileNameSaved string
		)
		nameSplit = strings.Split(strings.ToLower(headers[0].Filename), ".")
		if len(nameSplit) <= 1 {
			log.Println("ไฟล์ที่ส่งเข้ามาไม่ได้ระบุนามสกุลไฟล์")
			return UploadResultMessage{
				fileNameSaved: nil,
				message:       "ไฟล์ที่ส่งเข้ามาไม่ได้ระบุนามสกุลไฟล์",
				completed:     false,
			}
		}
		suffix = nameSplit[len(nameSplit)-1]
		switch {
		case suffix == "jpg" || suffix == "jpeg":
			uploadDaten, err = jpeg.Decode(imageFile)
			if err != nil {
				log.Println("jpeg.Decode() failed")
				return UploadResultMessage{
					fileNameSaved: nil,
					message:       "jpeg.Decode() failed",
					completed:     false,
				}
			}

		case suffix == "png":
			uploadDaten, err = png.Decode(imageFile)
			if err != nil {
				log.Println("png.Decode() failed")
				return UploadResultMessage{
					fileNameSaved: nil,
					message:       "png.Decode() failed",
					completed:     false,
				}
			}

		case suffix == "gif":
			uploadDaten, err = gif.Decode(imageFile)
			if err != nil {
				log.Println("gif.Decode() failed")
				return UploadResultMessage{
					fileNameSaved: nil,
					message:       "gif.Decode() failed",
					completed:     false,
				}
			}
		default:
			log.Println("unsupported image suffix")
			return UploadResultMessage{
				fileNameSaved: nil,
				message:       "unsupported image suffix",
				completed:     false,
			}
		}
		resized := resize.Resize(500, 0, uploadDaten, resize.Lanczos3)
		err = png.Encode(&bildBuf, resized)
		if err != nil {
			log.Println("png.Encode() failed")
			return UploadResultMessage{
				fileNameSaved: nil,
				message:       "png.Encode() failed",
				completed:     false,
			}
		}
		img := bildBuf.Bytes()
		fileNameSaved = genUlid()
		newFile, err = os.Create(revel.BasePath + "/public/uploads/users/" + fileNameSaved + ".png")
		if err != nil {
			log.Println("ไม่สามารถบันทึกข้อมูลรูปภาพได้")
			return UploadResultMessage{
				fileNameSaved: nil,
				message:       "ไม่สามารถบันทึกข้อมูลรูปภาพได้",
				completed:     false,
			}
		}
		defer newFile.Close()
		_, err = newFile.Write(img)
		if err != nil {
			log.Println("ไม่สามารถบันทึกข้อมูลรูปภาพได้")
			return UploadResultMessage{
				fileNameSaved: nil,
				message:       "ไม่สามารถบันทึกข้อมูลรูปภาพได้",
				completed:     false,
			}
		}
		return UploadResultMessage{
			fileNameSaved: &fileNameSaved,
			message:       "ทำการอัพโหลดรูปภาพสำเร็จ",
			completed:     true,
		}
	}
	log.Println("กรุณาตรวจสอบข้อมูลที่ส่งเข้ามาอีกครั้ง")
	return UploadResultMessage{
		fileNameSaved: nil,
		message:       "กรุณาตรวจสอบข้อมูลที่ส่งเข้ามาอีกครั้ง",
		completed:     false,
	}
}

// Index func
func (c App) Index() revel.Result {
	// secret, _ := revel.Config.String("app.secret")
	// encode := encrypt(secret, "world")
	// fmt.Println(encode)
	// fmt.Println(decrypt(secret, encode))
	// return c.Render()
	c.Response.Status = http.StatusAccepted
	data := JSONResult{
		Status:  c.Response.Status,
		Message: "Twinsynergy.co.th API Version 0.1",
	}
	c.Params.BindJSON(&data)
	return c.RenderJSON(data)
}

//ValidationUsernameAndPassword func
func (c App) ValidationUsernameAndPassword(username string, password string) bool {
	c.Validation.Required(username).Message("กรุณากรอกข้อมูล username")
	c.Validation.MaxSize(username, 50).Message("กรอกข้อมูล username ได้มากสุด 50 ตัวอักษร")
	c.Validation.MinSize(username, 5).Message("กรอกข้อมูล username อย่างน้อย 5 ตัวอักษร")
	c.Validation.Match(username, regexp.MustCompile("^\\w*$"))
	c.Validation.Required(password).Message("กรุณากรอกข้อมูล password")
	c.Validation.MinSize(password, 4).Message("กรอกข้อมูล username อย่างน้อย 4 ตัวอักษร")
	c.Validation.Match(password, regexp.MustCompile("^\\w*$"))

	if c.Validation.HasErrors() {
		return true
	}
	return false
}

// Login metthod
func (c App) Login(username string, password string) revel.Result {
	if c.ValidationUsernameAndPassword(username, password) {
		resultError := c.Validation.ErrorMap()
		var errText string
		for _, err := range resultError {
			errText = err.Message
			break
		}
		// log.Println(errText)
		// log.Println(result)
		return c.ReturnHeaderAndResponse(http.StatusNotAcceptable, errText, nil)
	}
	db := models.Gorm
	var user models.UserStruct
	db.Where("username = ?", username).First(&user)
	if user.Username != "" && user.Password != "" {
		if helpers.CheckPasswordHash(password, user.Password) {
			uid := strconv.Itoa(int(user.ID))
			secret, _ := revel.Config.String("app.secret")
			encode := encrypt(secret, uid)
			tokenString := helpers.EncodeTokenMobile(encode)
			auth := AuthenticationMobile{
				Token: tokenString,
			}
			userToken := &models.UserTokenStruct{
				Token:           tokenString,
				UserStructRefer: user.ID,
			}
			db.Create(&userToken)
			return c.ReturnHeaderAndResponse(http.StatusOK, "Login สำเร็จ", auth)
		}
		return c.ReturnHeaderAndResponse(http.StatusBadRequest, "Username และ Password ไม่ถูกต้อง", nil)
	}
	return c.ReturnHeaderAndResponse(http.StatusNotAcceptable, "กรุณากรอก Username และ Password โดยมีความยาวตั้งแต่ 4 - 15 ตัวอักษร", nil)
}

// Logout metthod
func (c App) Logout() revel.Result {
	tokenString, err := helpers.GetTokenString(c.Request.Header.Get("Authorization"))
	if err != nil {
		return c.ReturnHeaderAndResponse(http.StatusUnauthorized, err.Error(), nil)
	}
	var claims jwt.MapClaims
	claims, err = helpers.DecodeToken(tokenString)
	if err != nil {
		return c.ReturnHeaderAndResponse(http.StatusUnauthorized, err.Error(), nil)
	}
	// log.Println(claims)
	encode, found := claims["code"]
	if !found {
		return c.ReturnHeaderAndResponse(http.StatusUnauthorized, "Logout fails", nil)
	}

	str, _ := encode.(string)
	secret, _ := revel.Config.String("app.secret")
	uid := decrypt(secret, str)

	log.Println("encode string:", encode)
	log.Println("uid found:", uid)

	db := models.Gorm
	userToken := &models.UserTokenStruct{}
	db.Table("user_token_struct").Where("user_struct_refer = ?", uid).Where("token = ?", tokenString).First(&userToken)
	if userToken.ID != 0 {
		db.Exec("DELETE FROM user_token_struct WHERE id = ?", userToken.ID)
		return c.ReturnHeaderAndResponse(http.StatusOK, "Logout สำเร็จ", nil)
	}
	return c.ReturnHeaderAndResponse(http.StatusNotFound, "ไม่พบข้อมูลผู้ใช้งาน", nil)
}

// PostRegister func
func (c App) PostRegister(username string, password string, email string, description string, imageFile io.Reader) revel.Result {
	c.Validation.Required(username).Message("กรุณากรอกข้อมูล username")
	c.Validation.MaxSize(username, 50).Message("กรอกข้อมูล username ได้มากสุด 50 ตัวอักษร")
	c.Validation.MinSize(username, 5).Message("กรอกข้อมูล username อย่างน้อย 5 ตัวอักษร")
	c.Validation.Match(username, regexp.MustCompile("^\\w*$"))
	c.Validation.Required(password).Message("กรุณากรอกข้อมูล password")
	c.Validation.MinSize(password, 4).Message("กรอกข้อมูล username อย่างน้อย 4 ตัวอักษร")
	c.Validation.Match(password, regexp.MustCompile("^\\w*$"))
	c.Validation.Required(email).Message("กรุณากรอกข้อมูล email")
	c.Validation.Email(email).Message("กรุณากรอกข้อมูลให้เป็น email")
	c.Validation.Required(description).Message("กรุณากรอกข้อมูล description")
	if c.Validation.HasErrors() {
		resultError := c.Validation.ErrorMap()
		var errText string
		for _, err := range resultError {
			errText = err.Message
			break
		}
		// log.Println(errText)
		// log.Println(result)
		return c.ReturnHeaderAndResponse(http.StatusNotAcceptable, errText, nil)
	}

	db := models.Gorm

	var countUser int
	db.Table("user_struct").Where("username = ?", username).Count(&countUser)
	if countUser != 0 {
		return c.ReturnHeaderAndResponse(http.StatusBadRequest, "มี Username ("+username+") นี้ในระบบแล้ว", nil)
	}

	var countEmail int
	db.Table("user_struct").Where("email = ?", email).Count(&countEmail)
	if countEmail != 0 {
		return c.ReturnHeaderAndResponse(http.StatusBadRequest, "มี Email ("+email+") นี้ในระบบแล้ว", nil)
	}

	HashPassword, _ := helpers.HashPassword(password)
	var fileName = "default_users.jpg"
	user := &models.UserStruct{
		Username:    username,
		Password:    HashPassword,
		Email:       email,
		Description: description,
		PathImage:   fileName,
	}
	db.Create(&user)

	uploadResult := c.uploadImage(imageFile, c.Params.Files)
	if uploadResult.fileNameSaved != nil {
		fileName = *uploadResult.fileNameSaved + ".png"
		user.PathImage = fileName
		db.Save(&user)
	}

	uid := strconv.Itoa(int(user.ID))
	secret, _ := revel.Config.String("app.secret")
	encode := encrypt(secret, uid)
	tokenString := helpers.EncodeTokenMobile(encode)
	auth := AuthenticationMobile{
		Token: tokenString,
	}
	userToken := &models.UserTokenStruct{
		Token:           tokenString,
		UserStructRefer: user.ID,
	}
	db.Create(&userToken)

	return c.ReturnHeaderAndResponse(http.StatusOK, "สมัคร user สำเร็จ", auth)
}

//ReturnHeaderAndResponse func
func (c App) ReturnHeaderAndResponse(status int, message string, response interface{}) revel.Result {
	data := make(map[string]interface{})
	c.Response.Status = status
	data["header"] = JSONResult{
		Status:  c.Response.Status,
		Message: message,
	}
	if response != nil {
		data["response"] = response
	}

	c.Params.BindJSON(&data)
	return c.RenderJSON(data)
}

//Swagger func
func (c App) Swagger() revel.Result {
	path := revel.BasePath + "/public/uploads/"
	file, e := ioutil.ReadFile(path + "swagger.json")
	var jsonobject interface{}
	if e != nil {
		return c.RenderJSON(jsonobject)
	}
	json.Unmarshal(file, &jsonobject)
	return c.RenderJSON(jsonobject)
}
